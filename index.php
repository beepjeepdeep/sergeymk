<?php

$allYears = [];

function generate(int $number = 100): array
{
	$person = [];

	for ($i = 0; $i < $number; $i++) {
		$max = 2000;
		$min = 1900;

		$birth = mt_rand($min, $max);
		$death = mt_rand($birth, $max);

        array_push($person, [
            "deathYear"=> $death,
            'birthYear'=> $birth,
        ]);

    }
	return $person;
}

$people = generate(100);

echo '<pre>';

echo '<hr>';


foreach($people as $person) {
    for($year = $person['birthYear']; $year <= $person['deathYear']; $year++) {
        if (!array_key_exists($year, $allYears)) {
            $allYears[$year] = 0;
        }
        $allYears[$year] += 1; 
    }
}

arsort($allYears);
var_dump($allYears);

$mostYears = [];
$peopleMaxLive = $allYears[array_key_first($allYears)];


foreach($allYears as $year => $cnt) {
    if ($cnt == $peopleMaxLive) {
        $mostYears[] = $year;
    }
}
echo "<p>The most people lives ($peopleMaxLive) in this years: " .implode(',',$mostYears).'</p>';